# APLICACIONES SEGURAS EN LA NUBE - ACTIVIDAD 3.1 y 3.2

Autores: Manuel David García Cardenas & Manuel Navas Damas

Pila de desarrollo: Symfony, PHP7.2, Sqlite3, Nginx

Tutorial de uso:

1.- Lanzar el contenedor:
		- docker-compose up
		
2.- Acceder a la aplicación web en http://localhost:8888/

3.- Seleccionar el ejemplo a ejecutar:

	- Validación: introducir datos en los campos y seleccionar la opción segura ("Buscar") o la opción insegura ("Buscar sin validación") para comprobar el resultado. 
	
	- Inyección SQL: introducir el ID a buscar (p.e: 1) y seleccionar "Buscar" para realizar una consulta con protección frente a inyección SQL o seleccionar "Buscar sin validación" para realizar la consulta insegura (p.e: probar a introducir "1 OR 1=1", la consulta devolverá todos los datos en la base de datos).
	
	- XSS: del mismo modo que en el ejemplo de inyección SQL, pero si introducimos un script ejecutable (p.e: "<script> alert('Hola'); </script>") y seleccionamos la opción insegura el código se ejecutará, sin embargo si utilizamos la opción segura se tratará como texto y no se ejecutará.
	
Nota: Para el ejemplo XSS no utilizar el navegador Chrome, ya que implementa por defecto una protección XSS en el lado del cliente. En nuestras pruebas hemos utilizado el navegador Firefox.

Dirección web Docker Hub: https://hub.docker.com/r/manueldavidgc/act24mndmdgc

Dirección Google Cloud: http://104.154.176.11

# Referencias:
1.- Balsas, J.R. Creación y ejecución de contenedores. Universidad de Jaén, 2019

2.- Documentación oficial de Symfony: https://symfony.com/doc/current/index.html#gsc.tab=0

3.- Imagen Docker base: https://github.com/wyveo/nginx-php-fpm/tree/php72

