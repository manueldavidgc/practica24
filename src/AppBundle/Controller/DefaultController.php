<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Validacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/validacion", name="validacion")
     */
    public function validacionAction(Request $request)
    {
        $validacion = new Validacion();

        $form = $this->createFormBuilder($validacion)
            ->add('nombre', TextType::class)
            ->add('email', TextType::class)
            ->add('telefono', TextType::class)
            ->add('guardar', SubmitType::class)
            ->add('guardar sin validar', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $guardar = $form->getData();
            $validator = $this->get('validator');
            if ($form->get('guardar')->isClicked()) {
                if ($form->isValid() && $validator->validate($guardar)) {
                    try {
                        $this->getDoctrine()->getManager()->persist($guardar);
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('primary', 'Guardado correctamente');
                    } catch (\Exception $exception) {
                        $this->addFlash('danger', 'Hubo un error al guardar en la BBDD');
                    }
                } else {
                    $this->addFlash('danger', 'Entidad no valida');
                }

            } else if ($form->get('guardar sin validar')->isClicked()){
                try {
                    $this->getDoctrine()->getManager()->persist($guardar);
                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('primary', 'Guardado correctamente');
                } catch (\Exception $exception) {
                    $this->addFlash('danger', 'Hubo un error al guardar en la BBDD');
                }
            }
        }

        $todosLosDatos = $this->getDoctrine()->getRepository(Validacion::class)->findAll();
        
        // replace this example code with whatever you need
        return $this->render('default/validacion.html.twig', [
            'form' => $form->createView(),
            'todosLosDatos' => $todosLosDatos
        ]);
    }

    /**
     * @Route("/xss", name="xss")
     */
    public function xssAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('id', TextType::class)
            ->add('buscar', SubmitType::class)
            ->add('buscar sin validacion', SubmitType::class)
            ->getForm();

            $form->handleRequest($request);
    
            if ($form->isSubmitted()) {
                $data = $form->getData();
                $debeEjecutarse = true;
                if ($form->get('buscar')->isClicked()) {
                    $debeEjecutarse = false;
                }
            }
    
        return $this->render('default/xss.html.twig', [
            'form' => $form->createView(),
            'todosLosDatos' => $data,
            'debeEjecutarse' => $debeEjecutarse
        ]);
    }

    /**
     * @Route("/inyeccion", name="inyeccion")
     */
    public function inyeccionAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Validacion::class);
        $form = $this->createFormBuilder()
            ->add('id', TextType::class)
            ->add('buscar', SubmitType::class)
            ->add('buscar sin validacion', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->get('buscar')->isClicked()) {
                $datas = $repo->buscarPorIdConValidacion($data['id']);
            } else if($form->get('buscar sin validacion')->isClicked()) {
                $datas = $repo->buscarPorIdSinValidacion($data['id']);
            }
        }

        return $this->render('default/inyeccion.html.twig', [
            'form' => $form->createView(),
            'todosLosDatos' => $datas
        ]);
    }
}
